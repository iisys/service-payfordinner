<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
        <MutationEntry>
		    <MutatedBy></MutatedBy>
		    <Value></Value>
		    <Description></Description>
		    <Shares>
		      <xsl:for-each select="Subscription/Attendees/Attendee">
		      <Share>
		          <Entity><xsl:value-of select="Name"/></Entity>
		          <Portion><xsl:value-of select="Persons"/></Portion>
		      </Share>
		      </xsl:for-each>
		    </Shares>      
		</MutationEntry>
	</xsl:template>
</xsl:stylesheet>